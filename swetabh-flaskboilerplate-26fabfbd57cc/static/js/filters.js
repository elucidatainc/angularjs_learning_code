(
    function(){
        'use strict';
        angular.module('mainApp')
                .filter('checkmark',function(){
                    return function(input){
                         return input ? '\u2713' : '\u2718';     
                    };         
                });
      
        // ApplyFilter.$inject = [$filter]
        // function ApplyFilter(){
        //    return function(input){
        //         return input ? '\u2713' : '\u2718';     
        //    };
        // }
})();