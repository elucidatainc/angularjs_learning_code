(
    function(){
        
        'use strict';
        
       angular.module('customDirective')
              .directive('ngSparkline',sparkDirective);
  
        function sparkDirective(){
            var url = "http://api.openweathermap.org/data/2.5/forecast/daily?mode=json&units=imperial&cnt=14&callback=JSON_CALLBACK&q=";
            return{
                restrict : 'A',
                require  : '^ngCity',
                scope : {
                    ngCity : '@'
                },
                template : '<div class="sparkline"><h4>Weather for</h4><div class="graph"></div></div>',
                controller: sparkController,
                link : function (scope,iElement,iAttrs,ctrl){
                            scope.$watch('weather',function(newValue){
                            if(newValue){
                                var highs = [],
                                    height = iAttrs.height || 200,
                                    width = iAttrs.width || 80;
                            
                                angular.forEach(scope.weather, function(value){
                                   highs.push(value.temp.max);
                                });
                            }
                        });
                    } 
            }    
            // console.log(highs.length);
        }
        
        
        sparkController.$inject = ['$scope','$http'];
        
        function sparkController($scope,$http){
            $scope.getTemp = function(city) {
                    $http({
                    method: 'JSONP',
                    url: url + city
                    }).success(function(data) {
                    var weather = [];
                    angular.forEach(data.list, function(value){
                        weather.push(value);
                    });
                    $scope.weather = weather;
                    });
           }
        }
})();