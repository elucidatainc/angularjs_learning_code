(
    function(){
        
        'use strict';
        angular.module('customDirective')
                .directive("itemValue",checkDirective);
                
        function checkDirective(){
            return {
                restrict :'E',
                scope : {
                    like :'&'
                    // text : '='
                },
                template: '<input type="text" ng-model="starCount"/></br><input type="button" ng-click="like({star : starCount})" value="Like"/>'
           }
        }
})();