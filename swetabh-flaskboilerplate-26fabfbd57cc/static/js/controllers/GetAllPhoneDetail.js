(function(){
    
    'use strict';
    
    
    angular
       .module('mainApp.controllers',[])
       .controller('GetAllPhoneDetail',GetAllPhoneDetail);
       
       GetAllPhoneDetail.$inject = ['Phone'];
        
       function GetAllPhoneDetail(Phone){
                var getphonedetail = this;
                getphonedetail.phones = Phone.query();
                getphonedetail.orderProp='age';
        }
})();