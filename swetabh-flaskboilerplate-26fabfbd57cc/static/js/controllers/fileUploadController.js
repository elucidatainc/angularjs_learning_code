(
    function(){
        
        'use strict';
        angular.module('uploadApp')
               .controller('uploadController',uploadController);
        
        uploadController.$inject = ['$scope','Upload','$timeout'];
               
        function uploadController($scope,Upload,$timeout){
            // var vm = this;
            
            console.log("Upload controller calling");
            
            $scope.open = function() {
                $scope.showModal = true;
                
            };

            $scope.ok = function() {
                $scope.showModal = false;
                $scope.InVisible = true;
            };

            $scope.cancel = function() {
                $scope.showModal = false;
                $scope.InVisible = true;
            };
            
            $scope.uploadFiles = function(files, errFiles){
                
                $scope.InVisible = false;
                $scope.showModal = true;      
                $scope.files = files;
                $scope.errFiles = errFiles;
            
                if(files && files.length){
                    console.log(files.length);
                    angular.forEach(files, function(file){
                    var reader = new FileReader();
                    reader.onload = function(e){
                                // var file = (e.srcElement || e.target).files[0];
                                var csvval = e.target.result.split(/\r\n|\n/);
                                var headers = csvval[0].split(',');
                                var lines = [];
                                var count = 0;

                                for(var i=1;i<csvval.length;i++){
                                    
                                    if(i==1){
                                        console.log(""+i);
                                    }
                                    
                                    var data = csvval[i].split(',');
                                    
                                    if(data.length==headers.length){
                                        var tarr = [];
                                        for(var j = 0; j < headers.length ; j++){
                                            tarr.push(headers[j] + ":" + data[j]);
                                            if(data[j]=="" || data[j]=="NA"){
                                                count=count+1;
                                            }
                                        }
                                        lines.push(tarr);
                                    }
                                }
                            $scope.data = lines;
                        }
                        reader.readAsText(files[0]);
                    });
                }
            }
         }
})();