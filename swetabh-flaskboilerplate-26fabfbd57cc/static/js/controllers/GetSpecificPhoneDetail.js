(function(){
    
    'use strict';
    angular
        .module('mainApp.controllers')
        .controller('GetSpecificPhoneDetail',GetSpecificPhoneDetail);
        
        GetSpecificPhoneDetail.$inject = ['$routeParams','Phone'];
        
        function GetSpecificPhoneDetail($routeParams,Phone){
                
                var vm = this;
                // console.log($routeParams.phoneId);
                vm.phone = Phone.get(
                    {
                        phoneId : $routeParams.phoneId
                    }, 
                    function(phone){
                        vm.mainImageUrl = phone.images[0]; 
                    }
                );
                        
                vm.setImage = function(imageUrl){
                    vm.mainImageUrl = imageUrl;  
                };
        }
    
})();