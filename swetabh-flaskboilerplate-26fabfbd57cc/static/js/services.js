(
    function(){
    'use strict';
    
    angular.module('mainApp')
        .factory('Phone',Phone);
        
    Phone.$inject = ['$resource'];

    function Phone($resource){
        return $resource('/static/phones/:phoneId.json',{},{
            query : {method:'GET', params:{phoneId:'phones'}, isArray:true} 
        });    
    }
})();