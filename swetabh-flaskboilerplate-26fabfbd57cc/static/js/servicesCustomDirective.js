(
    function(){
        
        'use strict';
        angular.module('customDirective')
                .factory('getWeather',weatherService);
        
        weatherService.$inject = ['$scope','$http'];
        
       function weatherService($scope,$http,city){
       
            var url = "http://api.openweathermap.org/data/2.5/forecast/daily?mode=json&units=imperial&cnt=14&callback=JSON_CALLBACK&q=";
            $http({
                method : 'JSONP',
                url : url + city
            }).success(function(data){
              
                var weather = [];
                angular.forEach(data.list, function(value){
                    weather.push(value);
                })
                $scope.weather = weather;
            });
        }
        
})();