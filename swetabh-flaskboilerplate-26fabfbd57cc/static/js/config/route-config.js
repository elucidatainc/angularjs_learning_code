(function(){
        'use strict';
        
        angular
            .module('mainApp.route',['ngRoute'])
            .config(ProvidingRoute);
            
        var main = angular.module('mainApp.controllers');
        
        ProvidingRoute.$inject = ['$routeProvider'];
        
        function ProvidingRoute($routeProvider){
                $routeProvider
                    .when('/phones',{
                        templateUrl:'/static/js/partials/phone_list.html',
                        controller:main.GetAllPhoneDetail,
                        controllerAs:'gapd'
                    })
                    .when('/phones/:phoneId',{
                        templateUrl : '/static/js/partials/phone_details.html',
                        controller : main.GetSpecificPhoneDetail,
                        controllerAs : 'gspd'
                    }).
                    otherwise({
                        redirectTo : '/phones' 
                    });
        } 
          
})();