(function(){
    'use strict';
    
    angular.module('mainApp.route')
         .config(ChangeTag);
       
    //Changing the tag for Angular 
    function ChangeTag($interpolateProvider) {
        $interpolateProvider.startSymbol('{[');
        $interpolateProvider.endSymbol(']}');
    } 
        
})();