(
    function(){
        
       'use strict';
        
        angular.module('customDirective',[]);
     
        angular.module('customDirective').config(ChangeTag);
        
        ChangeTag.$inject = ['$interpolateProvider'];
       
        //Changing the tag for Angular 
        function ChangeTag($interpolateProvider) {
            $interpolateProvider.startSymbol('{[');
            $interpolateProvider.endSymbol(']}');
        } 
})();