(
    function() {
        'use strict';
        
      angular.module('mainApp',[
            'ngResource',
            'mainApp.controllers',
            'mainApp.route'
        ]);
})();