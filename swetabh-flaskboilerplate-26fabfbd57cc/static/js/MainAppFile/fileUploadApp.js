(
    function(){
        
        'use strict';
        
        angular.module('uploadApp',['ngFileUpload','ui.bootstrap.modal'])
                .config(ChangeTag);
        
        ChangeTag.$inject = ['$interpolateProvider'];
        
        function ChangeTag($interpolateProvider){
            $interpolateProvider.startSymbol('{[');
            $interpolateProvider.endSymbol(']}');
        }
        
           
})();