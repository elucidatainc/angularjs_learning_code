import datetime
import os

from flask import Blueprint,current_app,render_template, request,url_for,json,Flask,send_from_directory
from flask.ext.classy import FlaskView, route
from flask_application.controllers import TemplateView
from flask import jsonify

public = Blueprint('public', __name__)


app = Flask(__name__)

class QuotesView(FlaskView):
    '''IMPLEMENTS IMPORT EXPERIMENT VIEW'''
    route_base = '/quotes'
    # JSON_DATA = '/static/phones/phones.json'
   
    def index(self):
        return render_template('angularproject/index.html')
    
    @route('/custom_directive')
    def Custom_Directive(self):
        return render_template('angularproject/cus_directive.html')        
    
    @route('/file_upload')
    def File_Upload(self):
        return render_template('angularproject/file_upload.html')    
    @route('/happy', methods=['POST']) 
    def post(self):
        print request.get_json()
        #print(json)
        return request.data

        # rock = 
        # print "Rock On"+rock
        # return request

class IndexView(FlaskView, TemplateView):
    blueprint = public
    route='/'
    route_name = 'home'
    template_name = 'home/index.html'

    def get_context_data(self, *args, **kwargs):
        return {
            'now': datetime.datetime.now(),
            'config': current_app.config
        }
        
# class IndexView(TemplateView):
#     blueprint = public
#     route = '/upload'
#     route_name = 'Upload'
#     template_name = 'Custom/FileUpload.html'
       
#     def get_context_data(self, *args, **kwargs):
#         return {
#             # 'now' : datetime.datetime.now(),
#             # 'config' : current_app.config
#         }
