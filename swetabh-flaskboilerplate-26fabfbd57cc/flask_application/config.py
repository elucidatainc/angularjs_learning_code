import logging
import os
from pymongo.uri_parser import parse_uri


class Config(object):
        SECRET_KEY = '{SECRET_KEY}'
        SITE_NAME = 'Flask Site'
        SITE_ROOT_URL = 'http://example.com'
        LOG_LEVEL = logging.DEBUG

        MEMCACHED_SERVERS = ['localhost:11211']
        SYS_ADMINS = ['foo@example.com']

        # MYSQL support
        # Put MYSQL_ENV as path in .bashrc 
        # Eg. export MYSQL_ENV='mysql://root:password@localhost/temp'
        SQLALCHEMY_DATABASE_URI = os.environ['MYSQL_BOILERPLATE']
        
        # Configured for Gmail
        DEFAULT_MAIL_SENDER = 'Admin < username@example.com >'
        MAIL_SERVER = 'smtp.gmail.com'
        MAIL_PORT = 465
        MAIL_USE_SSL = True
        MAIL_USERNAME = 'username@gmail.com'
        MAIL_PASSWORD = '********'

        # Flask-Security setup
        SECURITY_EMAIL_SENDER = 'Security < security@example.com >'
        SECURITY_LOGIN_WITHOUT_CONFIRMATION = True
        SECURITY_REGISTERABLE = True
        SECURITY_RECOVERABLE = True
        SECURITY_URL_PREFIX = '/auth'
        SECUIRTY_POST_LOGIN = '/'
        SECURITY_PASSWORD_HASH = 'pbkdf2_sha512'
        # import uuid; salt = uuid.uuid4().hex
        SECURITY_PASSWORD_SALT = '2b8b74efc58e489e879810905b6b6d4dc6'

        # CACHE
        CACHE_TYPE = 'simple'

class ProductionConfig(Config):
    def __init__(self):
        super(ProductionConfig, self).__init__()
        self.ENVIRONMENT = 'Production'
        self.HEROKU = True
        self.PRODUCTION = True
        self.LOG_LEVEL = logging.INFO
        self.SERVER_NAME = 'example.com'

        self.MAIL_SERVER = 'smtp.mandrillapp.com'
        self.MAIL_PORT = 465
        self.MAIL_USE_SSL = True
        self.MAIL_USERNAME = os.getenv('MANDRILL_USERNAME')
        self.MAIL_PASSWORD = os.getenv('MANDRILL_APIKEY')


class DevelopmentConfig(Config):
    '''
    Use "if app.debug" anywhere in your code,
    that code will run in development mode.
    '''
    def __init__(self):
        super(DevelopmentConfig, self).__init__()
        self.ENVIRONMENT = 'Dev'
        self.DEBUG = True
        self.TESTING = False


class TestingConfig(Config):
    '''
    A Config to use when we are running tests.
    '''
    def __init__(self):
        super(TestingConfig, self).__init__()
        self.ENVIRONMENT = 'Testing'
        self.DEBUG = False
        self.TESTING = True


environment = os.getenv('ENVIRONMENT', 'DEVELELOPMENT').lower()
# Alternatively this may be easier if you are managing multiple aws servers:
# environment = socket.gethostname().lower()

if environment == 'testing':
    app_config = TestingConfig()
elif environment == 'production':
    app_config = ProductionConfig()
else:
    app_config = DevelopmentConfig()

